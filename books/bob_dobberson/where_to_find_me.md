### Where to find me
  - on matrix (matrix.org): `@bobdobberson:matrix.org`
  - on IRC (efnet.org): `dobberson`

I preach and teach on Facebook as `Pope Bob Dobberson` when I can't keep myself from it, I do not use it as a one-on-one discussion platform.
