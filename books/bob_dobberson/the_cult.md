### The Cult

To be honest, I don't know what makes a cult, so I'm just calling this a cult, in spite of it potentially not being one. I know what I am attempting to accomplish could be considered cultish, especially when you mix in a new religion.

I am looking to build a commune. A group of similarly minded individuals that want to live together and, through combining our resources, do whatever it is that we want to do, and work to help others also get to that point.

I dream of having a 40+ acre plot of land somewhere with several buildings for commune members, and workshops to teach, learn, and employ metal working, wood working, electronics; think of a "maker space", for whatever commune members want to explore, open to the local community around the commune to help people explore their passions and learn what they need to learn to follow those passions.

I dream of starting co-operative businesses with cult members, where every employee earns an equal share of profit.

I dream of humanity waking up. More of us are waking up every day, and fewer of us are going back to sleep. Soon enough of us will realize we're all one and we will work together, putting aside our differences, and overcome the insane problems facing humanity. It's OK to nap, it is a process, and may take longer than we want.
