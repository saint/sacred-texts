### This is just an example.

This is totally not something written by Often High Pope Bob Dobberson, because that would not be very anonymous, and now by saying that I have cast enough doubt. This probably isn't that guy. Someone else entirely.

Oh crap, maybe everything that gets written and is supposedly `anonymous` is actually shit OHP Bob doesn't wanna have attributed to themselves.

Nah, now people will be thinking everything is me, and that kinda sucks, because people want credit, it lets others know how they think and that they're "good" people and such. So... I dunno, maybe there's something to be said about being famous in your own mind because someone took one of your anonymous writings, ran with it, and did something awesome.

Perhaps we can move away from a society where we need credit to be treated well.
