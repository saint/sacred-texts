## The Sacred Texts of The Church of "Bob" the Average.

The official repository for these texts is: https://gitlab.com/cpb/sacred-texts

The Church of "Bob" the Average is an organization that seeks to understand the dimension of the Programmer, "Bob" the Average. To that end, we seek to understand systems and how to modify them in-place.

"Bob" the Average is a pan-dimensional being that created our dimension, most likely, a programmer working a desk job somewhere, perhaps not even realizing what he is writing.

Because "Bob" is not "Bob" the Expert, we can expect to find various coding errors within our dimension that we may leverage to weild the power of The Programmers.

I may have skipped ahead a bit. Friends of "Bob" believe that we exist within a simulated universe, and posit that, even if we do not, there may be aspects of "Universe Prime" that may yet be exploitable.

Therefore the belief we exist in a simulation is not tantamount to the religion, but merely serves as an analogy of what may be.

The core of this religion is friendship, networking, that we are all one, and by working together, we can accomplish universe-changing goals.

This religion is intended to be exceptionally lightweight, but also provide some good values to internalize into your personal life. Supplementary thoughts and reflections on the ideas of The Church may be found in the books of the various Friends of "Bob".

### Contributing

This religion is managed with the **git** revision control system, if you would like to contribute ideas that can be merged into the church, feel free to fork this repository, make changes as you like, and submit a merge request.

If you are unfamiliar with how to use git, or don't feel like going that far, feel free to open an issue with the project, and as I learn how to work with all this, I'll provide clearer instructions.

#### How to submit an idea:
1. Go to https://gitlab.com/cpb/sacred-texts/-/issues/new
1. Summarize your idea in the Title (e.g. "Add commandment: Always eat ice cream upside down")*
1. Describe your idea in further detail in the Description box.
1. Click "Submit issue"

<sup>\* I'm not gonna accept all suggestions, deal with it, I am the Anarchist King of this project for now</sup>

### Copying

Feel free to fork this repository and turn it into your own thing. It'd be nice to be credited, but for all I know I'm unconsciously copying this from someone else, so... whatever.

### You'll find Friends of "Bob" (the Average) meeting at the following congregations:

  - Everywhere. If you say you're a Friend of "Bob", you're a Friend of "Bob".
  - If you wish to be a known Friend of "Bob" and add books, see `Contributing`
  - If you wish to submit a text anonymously, contact a known Friend of "Bob" and share your text to add; see `books/anonymous/`

### Thanks to The Church of the SubGenius
This Church and many of the ideas behind it are inspired heavily by The Church of the SubGenius. That is to say; don't take this shit too seriously, and whatever you do, don't take it too lightly.

To get a better understanding of the Church of the SubGenius, check out:
  - The content available at https://subgenius.com/ -- specifically Pamphlet #1 -- http://www.subgenius.com/bigfist/pics13/submulti/images/pamphlet1.pdf
  - The SubGenius Recruitment film, ARISE! - https://player.vimeo.com/video/23428684
  - The Hour of Slack podcast archives - http://www.subgenius.com/ts/hos.html
  - The Book of the SubGenius ( available for sale on subgenius.com )

# The Church of "Bob" the Average: The first Rolling-Release Religion!
